#!/bin/bash

# Robin Trietsch's dotfiles
# install script

# -------------------------------------------------------------------------
# Parameters
# -------------------------------------------------------------------------
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
RECIPES="$DIR/setup/.brew_recipes"
CASKS="$DIR/setup/.brew_casks"
MAS_APPS="$DIR/setup/.mas_apps"
PREFERENCES="$DIR/setup/.preferences"
PLIST_SETTINGS="$DIR/setup/.macos"
WGETRC="$DIR/setup/.wgetrc"
HEADERS_COLORS="$DIR/setup/.headers_colors"
SSH_FOLDER="$DIR/setup/ssh"

# Set headers and colors
source "$HEADERS_COLORS"

# -------------------------------------------------------------------------
# Install Homebrew
# -------------------------------------------------------------------------

e_header "Installing Homebrew"
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
e_success "Done installing Homebrew."

# -------------------------------------------------------------------------
# Install yadr
# -------------------------------------------------------------------------

e_header "Installing YADR"
/bin/sh -c "`curl -fsSL https://raw.githubusercontent.com/skwp/dotfiles/master/install.sh `"
e_success "Done installing YADR."

# -------------------------------------------------------------------------
# Install Brew recipes
# -------------------------------------------------------------------------

e_header "Installing Homebrew recipes"
/bin/bash "$RECIPES"
e_success "Done installing Homebrew recipes"

# -------------------------------------------------------------------------
# Install Brew Casks
# -------------------------------------------------------------------------

e_header "Installing Homebrew casks"
/bin/bash "$CASKS"
e_success "Done installing Homebrew casks"

# -------------------------------------------------------------------------
# Set preferences / aliases
# -------------------------------------------------------------------------

e_header "Adding aliases, functions, and preferences to zprofile"
gsed  -i '1i # ------------------------------------------- \
# Personal \
# ------------------------------------------- \
source "/Users/'"${USER}"'/git/dotfiles/.aliases" \
source "/Users/'"${USER}"'/git/dotfiles/.functions" \
source "/Users/'"${USER}"'/git/dotfiles/.preferences" ' "/Users/$USER/.zprofile"
e_success "Done adding to zprofile."

# -------------------------------------------------------------------------
# Set Apple plists
# -------------------------------------------------------------------------

e_header "Modifying System Preferences"
/bin/bash "$PLIST_SETTINGS"
e_success "Done modifying System Preferences."

# -------------------------------------------------------------------------
# Copy wgetrc to home
# -------------------------------------------------------------------------

e_header "Adding wgetrc to home."
cp $WGETRC /Users/$USER/
e_success "Done adding wgetrc home."

# -------------------------------------------------------------------------
# Copy ssh to /etc/ssh
# -------------------------------------------------------------------------

e_header "Copying ssh to /etc/ssh"
cp $SSH_FOLDER /etc/ssh
e_success "Done copying ssh to /etc/ssh."
